import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { IgxDatePickerComponent, IgxInputDirective } from 'igniteui-angular';

@Component({
  selector: 'app-global-information',
  templateUrl: './global-information.component.html',
  styleUrls: ['./global-information.component.scss']
})
export class GlobalInformationComponent implements OnInit {
  businessRequestor: FormGroup;
  public startDate: Date = new Date(Date.now());

  constructor(private formBuilder: FormBuilder) {
    this.businessRequestor = new FormGroup({
      project: new FormGroup({
        id: new FormControl(''),
        title: new FormControl('')
      }),
      requestor: new FormGroup({
        name: new FormControl(''),
        site: new FormGroup({
          city: new FormControl(''),
          country: new FormControl('')
        }),
        phoneNumber: new FormControl(''),
        email: new FormControl(''),
        department: new FormControl(''),
        priority: new FormControl('')
      }),
      description: new FormControl(''),
      businessMotivation: new FormControl(''),
      date: new FormGroup({
        startDate: new FormControl(new Date()),
        deliveryDate: new FormControl(new Date())
      }),
      costEstimate: new FormGroup({
        hardware: new FormControl(''),
        software: new FormControl(''),
        maintenance: new FormControl(''),
        service: new FormControl('')
      }),
      roiCalculation: new FormControl(''),
      comments: new FormControl('')
    });
  }

  ngOnInit() {}

  public textArea(igxInput: IgxInputDirective) {
    igxInput.isTextArea = true;
  }

  public monthsView(datePicker: IgxDatePickerComponent) {
    datePicker.calendar.activeViewYear();
  }

  public yearsView(datePicker: IgxDatePickerComponent) {
    datePicker.calendar.activeViewDecade();
  }

  get formData() { return this.businessRequestor.get('Data'); }
}
