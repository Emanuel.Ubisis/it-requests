import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  IgxButtonModule,
  IgxIconModule,
  IgxLayoutModule,
  IgxNavigationDrawerModule,
  IgxRippleModule,
  IgxToggleModule,
  IgxInputGroupModule,
  IgxDatePickerModule,
  IgxTimePickerModule,
  IgxComboModule,
  IgxSelectModule,
  IgxDividerModule,
  IgxAvatarModule,
  IgxCardModule,
  IgxChipsModule,
  IgxListModule,
  IgxExpansionPanelModule,
  IgxTabsModule,
  IgxSliderModule
} from 'igniteui-angular';
import { GlobalInformationComponent } from './global-information/global-information.component';
import { ItValidationComponent } from './it-validation/it-validation.component';
import { ProjectAssignmentsComponent } from './project-assignments/project-assignments.component';
@NgModule({
  declarations: [AppComponent, GlobalInformationComponent, ItValidationComponent, ProjectAssignmentsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    IgxButtonModule,
    IgxIconModule,
    IgxLayoutModule,
    IgxNavigationDrawerModule,
    IgxRippleModule,
    IgxToggleModule,
    IgxInputGroupModule,
    IgxDatePickerModule,
    IgxTimePickerModule,
    IgxComboModule,
    IgxSelectModule,
    IgxDividerModule,
    IgxAvatarModule,
    IgxCardModule,
    IgxChipsModule,
    IgxListModule,
    IgxExpansionPanelModule,
    IgxSliderModule,
    BrowserAnimationsModule,
    IgxTabsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
