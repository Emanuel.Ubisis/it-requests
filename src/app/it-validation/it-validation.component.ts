import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-it-validation',
  templateUrl: './it-validation.component.html',
  styleUrls: ['./it-validation.component.scss']
})
export class ItValidationComponent implements OnInit {
  itValidation: FormGroup;
  constructor() {
    this.itValidation = new FormGroup({
      project: new FormGroup({
        id: new FormControl(''),
        title: new FormControl('')
      }),
      requestor: new FormGroup({
        name: new FormControl(''),
        site: new FormGroup({
          city: new FormControl(''),
          country: new FormControl('')
        }),
        phoneNumber: new FormControl(''),
        email: new FormControl(''),
        department: new FormControl(''),
        priority: new FormControl('')
      }),
      description: new FormControl(''),
      businessMotivation: new FormControl(''),
      date: new FormGroup({
        startDate: new FormControl(new Date()),
        deliveryDate: new FormControl(new Date())
      }),
      costEstimate: new FormGroup({
        hardware: new FormControl(''),
        software: new FormControl(''),
        maintenance: new FormControl(''),
        service: new FormControl('')
      }),
      roiCalculation: new FormControl(''),
      comments: new FormControl('')
    });
  }

  ngOnInit() {}
}
