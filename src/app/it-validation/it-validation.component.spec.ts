import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItValidationComponent } from './it-validation.component';

describe('ItValidationComponent', () => {
  let component: ItValidationComponent;
  let fixture: ComponentFixture<ItValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
